#include <WiFi101.h>

/*
 * The4BEES example sketch. It reads the sensors:
 * - ControlEverything HDC1000 (T and H)
 * - ControlEverything ISL29003 (Light)
 * - ams IAQ-Core (VOC)
 * - Telaire T6713 (carbon dioxide)
 * 
 * creates a Yucca JSON (SmartDataPlatform) and sends it through a Wi-Fi network. 
 * This code has been tested with a WPA connection. The system time is updated using
 * a NTP server and data are not sent until the system time is not updated.
 * 
 * Change the 'define' values putting:
 * - the WiFi parameters of your network 
 * - the your YUCCA configuration data.
 * - T/H sensor used.
 * - DHCP or static address
 * 
 * Author:
 * Marco Boeris Frusca
 * 
 * Date:
 * 2018-01-19
 * 
 * Version: 1.2.2
 * 
 * Change:
 * - Fix error compiling with Ethernet
 */



/**************************************************************************************
 * INLUDE AND DEFINE
 **************************************************************************************/
// Use WiFi connection. Comment it to use Ethernet 
#define WIFI 1

// Enable SAM21 Watchdog
#define WATCHDOG 0

#ifdef WIFI
// Wi-FI SSID
#define WIFI_SSID    "WIFI"

// Wi-FI keyword
#define WIFI_KEYWORD "PASSWORD"

// Use DHCP
#define WIFI_DHCP 1

#endif

// Use sensor SI7020
#define USE_CE_SI7020 1

// Yucca SmartObject
#define YUCCA_OBJ_UUID   "029de772-4825-49f0-d502-2d9c72a228e2"
#define YUCCA_OBJ_STREAM "fr69_hespul_strm_sn1"
#define YUCCA_OBJ_ID     "HESPUL"
#define YUCCA_OBJ_TENANT "the4bees"


// ---------------------------------------------------------------------------------------------------------------------------------------- 
// YUCCA_OBJ_STREAM      nom du flux                     YUCCA_OBJ_UUID                        Carte arduino                 Attribuée à
// ---------------------------------------------------------------------------------------------------------------------------------------- 
// fr69_hespul_strm_sn1  Fr69-hespul-The4Bees-StrmSN1    029de772-4825-49f0-d502-2d9c72a228e2  Fr69-hespul-The4Bees-BoxSN1   HESPUL
// fr69_hespul_strm_sn2  Fr69-hespul-The4Bees-StrmSN2    fd06db80-189e-4af8-b95a-6ed6308fce3c  Fr69-hespul-The4Bees-BoxSN2   Maison de l'environnement - Ethernet
// fr69_hespul_strm_sn3  Fr69-hespul-The4Bees-StrmSN3    dce25770-7397-472e-9952-e6bf83b912f5  Fr69-hespul-The4Bees-BoxSN3   Maison de l'environnement - Ethernet
// fr69_hespul_strm_sn4  Fr69-hespul-The4Bees-StrmSN4    8743dfcd-2e0a-4e47-9a27-c2e28d829a56  Fr69-hespul-The4Bees-BoxSN4   Lycée la Martinière - The4Bees-wifi1
// fr69_hespul_strm_sn5  Fr69-hespul-The4Bees-StrmSN5    a3a96be8-2108-4cdb-899b-d07ce34fde3a  Fr69-hespul-The4Bees-BoxSN5   Lycée la Martinière - The4Bees-wifi2
// fr69_hespul_strm_sn6  Fr69-hespul-The4Bees-StrmSN6    8a36862b-87e2-4d9c-94fd-26407057ac78  Fr69-hespul-The4Bees-BoxSN6   Maison du numérique
// fr69_hespul_strm_sn7  Fr69-hespul-The4Bees-StrmSN7    7063564b-9657-4cc8-e6ab-48e5caca106f  Fr69-hespul-The4Bees-BoxSN7   École Berthelier - The4Bees-wifi3
// fr69_hespul_strm_sn8  Fr69-hespul-The4Bees-StrmSN8    670773b8-b1d4-41b1-9afc-8474183fa011  Fr69-hespul-The4Bees-BoxSN8   École Berthelier - The4Bees-wifi3
// fr69_hespul_strm_sn9  Fr69-hespul-The4Bees-StrmSN9    fedd3b2f-35ee-4ba7-85a5-53c9f047f5fb  Fr69-hespul-The4Bees-BoxSN9   MFR
// fr69_hespul_strm_s10  Fr69-hespul-The4Bees-StrmSN10   1292f1e1-a65d-442a-e7fc-b42e315ab9fe  Fr69-hespul-The4Bees-BoxSN10  Log Pédago 
// fr69_hespul_strm_s11  Fr69-hespul-The4Bees-StrmSN11   da6ef1ac-8c3f-4ec8-d56f-a930cc1391b4  Fr69-hespul-The4Bees-BoxSN11   
// ---------------------------------------------------------------------------------------------------------------------------------------- 


// Yucca Server
#define YUCCA_SERVER_ADDR  "stream.smartdatanet.it"
#define YUCCA_SERVER_PORT  80
#define YUCCA_SERVER_PAGE  "/api/input/"

// Yucca User
#define YUCCA_USERNAME  "the4bees"
#define YUCCA_PASSWORD  "dGhlNGJlZXM6WUtlMk9FdzAyNQ==" 

// Serial Connection
#define BAUDRATE 115200

// Sample Time (milliseconds)
#define T_SAMPLE 60000

// Watchdog Timeout
#define T_WATCHDOG 16000

// NTP  attempt
#define NTP_ATTEMPT 3

// Verbose level (0: STD_OUTPUT, 1: INFO, 2: DEBUG)
#define VERBOSE_LEVEL 2

#include <Wire.h>
#include <SPI.h>
#include <SD.h>

#ifdef WIFI
#include <WiFi101.h>
#include <WiFiUdp.h>
#else
#include <Ethernet2.h>
#include <EthernetUdp2.h>
#endif

#include <TimeLib.h>

#ifdef WATCHDOG 
// Watchdog
#include <Adafruit_SleepyDog.h>
#endif


/**************************************************************************************
 * ENUM AND TYPE DEFINITION
 **************************************************************************************/

/** 
 * Enumeration for PIN mapping 
 */
enum _PIN_NUMBER 
{
  //! Red LED 
  LED_RED = 6, 

  //! Green LED
  LED_GREEN = 8, 
  
  //! Blue LED
  LED_BLUE = 9, 
  
  //! Blue LED
  LED_3 = ATN, 

  //! WiFi chip enable
  WIFI_EN = 0, 

  //! WiFi reset
  WIFI_RESET = 5, 

  //! WiFi IRQ
  WIFI_INT = 7, 

  //! WiFi chip select (low active)
  WIFI_CSN = 10, 
  
  //! SD chip select
  SD_CS = 3, 

  //! SD card detect
  SD_CD = 2
};

/**
 * Enumeratio for the I2C address map
 */
enum _I2C_ADDRESS {  
  //! Control Everything HDC1000 (Temperature and Humidity)
  CE_HDC1000 = 0x40, 

  //! Control Everything SI7020-A20 (Temperature and Humidity)
  CE_SI7020 = 0x40, 

  //! Control Everything ISL29003 (Lighting)
  CE_ISL29003 = 0x44, 

  //! IAC core (VOC)
  IAQ = 0x5A, 

  //! T6713 (CO2)
  T6713 = 0x15
};

/**
 * Measurement: it is composed of a time reference and the set of the measurements.
 * 
 */
typedef struct _measure {
  //! Timestamp
  time_t ts;

  //! Temperature
  float temperature;

  //! Humidity
  float humidity;

  //! Light
  float light;

  //! Volatile Organic Compounds (VOC)
  float voc;

  //! Carbon dioxide (CO2)
  float co2;
} Measure;

/**
 * Yucca smart-object description.
 */
typedef struct _yucca_obj {
  //! UUID
  String uuid;

  //! UUID
  String tenant;

  //! UUID
  String stream;

  //! Identifier
  String id;
} YuccaObj;

/**
 * Yucca server description.
 */
typedef struct _yucca_server {
  //! Address
  String addr;

  //! Port number
  uint16_t port;

  //! Service page
  String service;
} YuccaServer;


/**
 * Yucca user description.
 */
typedef struct _yucca_user {
  //! User
  String username;

  //! Service page
  String password;
} YuccaUser;


/**************************************************************************************
 * CONSTANT
 **************************************************************************************/
/// Size of the line buffer
const uint8_t LENGTH_LINE = 80;


/**************************************************************************************
 * GLOBAL VARIABLE
 **************************************************************************************/
//-------------------------------- Debug ------------------------------------
// Uptime in milliseconds (0 if not initialized)
unsigned long uptime = 0;

/// Debug level
uint8_t verbose_level = 0;

//--------------------------------- SD Card ---------------------------------
// Initialize the SD library
/// SD card 
Sd2Card card;

/// Volume of the SD card 
SdVolume volume;

/// Root file-system of the SD card 
SdFile root;


//--------------------------------- Network ---------------------------------
#ifdef WIFI
/// Network SSID (name)
char ssid[] = WIFI_SSID;     //  your network SSID (name)

/// Network password
char pass[] = WIFI_KEYWORD;  // your network password

/// WiFi radio's status
int status = WL_IDLE_STATUS;     
#endif

#ifdef WIFI
/// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;

/// WiFi client on port 80
WiFiClient network_client;
#else
/// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

/// Ethernet client on port 80
EthernetClient network_client;

// Newer Ethernet shields have a MAC address printed on a sticker on the shield
/// MAC address for ethernet controller
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };

#endif

//IPAddress ip(192, 168, 0, 177);
//IPAddress gw(192, 168, 0, 254);
//IPAddress dns1(192, 168, 0, 254);
//IPAddress subnet(255, 255, 255, 0);

IPAddress ip(130, 192, 69, 235);
IPAddress gw(130, 192, 69, 254);
IPAddress dns1(130, 192, 68, 9);
IPAddress subnet(255, 255, 255, 0);

/// Flag for network present and working correctly
bool network_enabled = false;

//------------------------------- NTP Server --------------------------------
/// NTP Port
unsigned int localPort = 2390;      // local port to listen for UDP packets

/// NTP packet size
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

/// NTP packet buffer
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

/// NTP server
IPAddress timeServer(193, 204, 114, 105); // ntp.ien.it NTP server

/// Network connected flag
bool network_connected = false;

/// Time NTP updating
time_t tnpt = 0;


//--------------------------------- Sensors ---------------------------------



//---------------------------------- YUCCA ----------------------------------
/// Smart Object
YuccaObj smartobj;

/// Server
YuccaServer server;

/// User for sending data to the server
YuccaUser user;

// Unsent measures
uint8_t unsent = 0;

//--------------------------------- Watchdog --------------------------------
/// Watchdog timeout in milliseconds
#ifdef WATCHDOG
uint16_t t_watchdog = T_WATCHDOG;
#else
uint16_t t_watchdog = 0;
#endif



/**************************************************************************************
 * FUNCTION DECLARATION
 **************************************************************************************/

//---------------------------------- LEDs -----------------------------------
/**
 * Blinks a LED: it turns the LED on for 'time_ms' milliseconds.
 * 
 * \param[in] pin_number : pin there the led is connected
 * \param[in] time_ms : time in milliseconds in which the LED is turned on.
 * 
 */
void blink_led (uint8_t pin_number, uint16_t time_ms);

/**
 * Blinks the LED_3: it turns the LED on for 'time_ms' milliseconds.
 * 
 * \param[in] time_ms : time in milliseconds in which the LED is turned on.
 * 
 */
void blink_led3 (uint16_t time_ms);
 
 /**
  * Tests RGB LED, turning on and then off red, green and blue colours.
  * 
  */
void test_rgb();

//--------------------------------- Sensors ---------------------------------
/**
 * Initializes the Control Everythings SI7020 sensor (T/H).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool initalize_CE_SI7020();


/**
 * Reads the Control Everythings SI7020 sensor (T/H). It can read either temperature 
 * or relative humidity value or both.
 * 
 * \param[out] temperature : temperature value
 * \param[out] humidity : relative humidity value
 * \param[in] sensor : type of sensor to read. Possible values are: 0 (temperature and relative humidity, 1 (temperature) and 2 (relative humidity).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool read_CE_SI7020(float &temperature, float &humidity, uint8_t sensor = 0);

/**
 * Initializes the Control Everythings HDC1000 sensor (T/H).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool initalize_CE_HDC1000();

/**
 * Reads the Control Everythings HDC1000 sensor (T/H). It can read either temperature 
 * or relative humidity value or both.
 * 
 * \param[out] temperature : temperature value
 * \param[out] humidity : relative humidity value
 * \param[in] sensor : type of sensor to read. Possible values are: 0 (temperature and relative humidity, 1 (temperature) and 2 (relative humidity).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool read_CE_HDC1000(float &temperature, float &humidity, uint8_t sensor = 0);

/**
 * Initializes the Control Everythings ISL29003 sensor (Light). It sets the sensor to measure only  
 * the visible spectrum (Diode1 - Diode2)
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool initalize_CE_ISL29003();

/**
 * Reads the Control Everythings ISL29003 sensor (Light). 
 * 
 * \param[out] light : light value
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool read_CE_ISL29003(float &light);

/**
 * Initializes the IAQ-Core sensor (VOC). It tries to read a value.

 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool initalize_IAQCore();

/**
 * Reads the IAQ-Core sensor (VOC).
 * 
 * \param[out] voc : VOC value in equivalent carbon dioxide (ppm).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool read_IAQCore(float &voc);

/**
 * Initializes the T6713 sensor (CO2). It tries to read a value.

 * 
 * return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool initalize_T6713();

/**
 * Reads the T6713 sensor (CO2). 
 * 
 * \param[out] co2 : carbon dioxide concentration (ppm).
 * 
 * \return false if sensor is not connected or there is an I2C read/write error. True otherwise.
 */
bool read_T6713(float &voc);


#ifdef WIFI
//---------------------------------- WiFi -----------------------------------
/**
 * Enables the WINC1500.
 */
bool wifi_turn_on();

/**
 * Disable the WINC1500
 */
void wifi_turn_off();

/**
 * Reset the WINC1500.
 */
void wifi_reset();

/**
 * Tries to connect to a WPA or an OPEN network. If the field 'password is empty, it means that the
 * WiFi is OPEN, otherwise WPA/WPA2.
 * 
 * \param[in] ssid : network name.
 * \param[in] password : password or keyword for the WAP/WPA2 network.
 * 
 * \return true if it is connected to the network, false otherwise.
 */
bool wifi_connect(char * ssid, char* password);

/**
 * Prints on serial console the network parameter for the WiFi connection.
 */
void printWiFiData();

/**
 * Prints on serial console information about the current WiFi network.
 */
void printCurrentNet();
#endif

//--------------------------------- Utils -----------------------------------
/**
 * Prints on the serial console a number padding with 0 id necessary (e.g. 7 => 07).
 * 
 * \param[in] digits : number to padding.
 */
void printDigits(int digits);


//-------------------------------- Debug ------------------------------------
/**
 * Print the system uptime as number of seconds.
 * 
 */
void print_uptime();

//--------------------------------- NTP -------------------------------------
/**
 * Sends a NTP request to the NTP server
 * 
 * \param[in] address : NTP server IP address.
 */
void sendNTPpacket(IPAddress& address);

/**
 * Gets the current time in epoch format, using a server NTP.
 * 
 * \return current time in epoch time or 0 if errors.
 */
time_t getNtpTime();

/**
 * Update the system time using a NTP server.
 * 
 * \return false if there is an error, true otherwise.
 */
bool ntpupdate();


//--------------------------------- YUCCA -----------------------------------
/**
 * Creates the JSON to send to SDP platform.
 * 
 * \param[out] json : reference to the string that will contain the JSON.
 * \param[in] m : masurements to send
 * \param[in] smartobj : SDP smart-object.
 */
void create_json(String &json, Measure &m, YuccaObj &smartobj);

/**
 * Send a JSON to the SDP platform.
 * 
 * \param[in] server : SDP server.
 * \param[in] user : SDP user.
 * \param[in] smartobj : SDP smart-object
 * \param[in] msg : Message or JSON to send.
 * 
 * \return false if error, true otherwise.
 */
bool yucca_send(YuccaServer &server, YuccaUser &user, YuccaObj &smartobj, String &msg);


//------------------------------- Watchdog ----------------------------------
/**
 * Enables the watchdog timer to reset the machine after a period of time
 * without any calls to reset().  The passed in period (in milliseconds)
 * is just a suggestion and a lower value might be picked if the hardware    
 * does not support the exact desired value.
 * 
 * \param[in] t : watchdog timeout
 * 
 * \return effective watchdog timer
 */
uint16_t watchdog_init(uint16_t t);

/**
 * Resets the watchdog.
 */
void watchdog_reset();



/**************************************************************************************
 * SETUP
 **************************************************************************************/
// The setup routine runs once when you press reset
void setup() {
  // Initialise I2C communication as MASTER
  Wire.begin();

  // Initialize serial communication at 9600 bits per second
  Serial.begin(BAUDRATE);
  Serial.println( );
  Serial.println( F("___________.__              _______________                      ") );
  Serial.println( F("\\__    ___/|  |__   ____   /  |  \\______   \\ ____   ____   ______") );
  Serial.println( F("  |    |   |  |  \\_/ __ \\ /   |  ||    |  _// __ \\_/ __ \\ /  ___/") );
  Serial.println( F("  |    |   |   Y  \\  ___//    ^   /    |   \\  ___/\\  ___/ \\___ \\ ") );
  Serial.println( F("  |____|   |___|__/\\_____>____   ||________/\\_____>\\_____>______>") );
  Serial.println( F("                              |__|                               ") );


  
  
  
  
  Serial.println( F("THE4BEES system boot\n") );

  watchdog_init(t_watchdog);

  // Initialize LEDs
  watchdog_reset();
  Serial.print ( F("Initialing RGB LED pins [D") );
  Serial.print ( LED_RED );
  Serial.print ( F(" : Red, D") );
  Serial.print ( LED_GREEN );
  Serial.print ( F(" : Green, D") );
  Serial.print ( LED_BLUE );
  Serial.print ( F(" : BLUE] ... ") );
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  test_rgb();
  Serial.println ( F("done") );

  Serial.print ( F("Initialing LED3 pin [D") );
  Serial.print ( LED_3 );
  Serial.print ( F("] ... ") );
  pinMode(LED_3, OUTPUT);
  Serial.println ( F("done") );

  watchdog_reset();
  blink_led3(2000);

  watchdog_reset();
  // Initialize Control Everything HDC1000 sensor [T/H] 
#ifdef USE_CE_SI7020
  Serial.print ( F("Initialing Control Everything SI702 sensor [T/H] ... ") );
  if ( initalize_CE_SI7020() )
#else
  Serial.print ( F("Initialing Control Everything HDC1000 sensor [T/H] ... ") );
  if ( initalize_CE_HDC1000() )
#endif
//  Serial.print ( F("Initialing Control Everything HDC1000 sensor [T/H] ... ") );
//  if ( initalize_CE_HDC1000() )
  {
    Serial.println ( F("done") );
  }
  else
  {
    Serial.println ( F("error") );
  }

  watchdog_reset();
  // Initialize Control Everything HDC1000 sensor [T/H] 
  Serial.print ( F("Initialing Control Everything ISL29003 sensor [Light] ... ") );
  if ( initalize_CE_ISL29003() )
  {
    Serial.println ( F("done") );
  }
  else
  {
    Serial.println ( F("error") );
  }

  watchdog_reset();
  // Initialize IAQ-Core sensor [VOC]
  Serial.print ( F("Initialing IAQ-Core sensor [VOC] ... ") );
  if ( initalize_IAQCore() )
  {
    Serial.println( F("done") );
  }
  else
  {
    Serial.println( F("error") );
  }
  
  watchdog_reset();
  // Initialize T6713 sensor [CO2]
  Serial.print ( F("Initialing T6710 sensor [CO2] ... ") );
  if ( initalize_T6713() )
  {
    Serial.println( F("done") );
  }
  else
  {
    Serial.println( F("error") );
  }


  watchdog_reset();
  Serial.print ( F("\nLoading configuration ... ") );
  // Initialize Yucca description 
  smartobj.uuid   = YUCCA_OBJ_UUID;
  smartobj.tenant = YUCCA_OBJ_TENANT;
  smartobj.stream = YUCCA_OBJ_STREAM;
  smartobj.id     = YUCCA_OBJ_ID;

  user.username = YUCCA_USERNAME;
  user.password = YUCCA_PASSWORD;

  server.addr = YUCCA_SERVER_ADDR;
  server.port = YUCCA_SERVER_PORT;
  server.service = YUCCA_SERVER_PAGE;
  server.service += user.username;
  server.service += "/";
  Serial.println( F("done\n") );

/*
  Serial.println( F("Yucca Smart-object") );
  Serial.print( F("UUID  : ") );
  Serial.println( smartobj.uuid );
  Serial.print( F("Tenant : ") );
  Serial.println( smartobj.tenant );
  Serial.print( F("Stream : ") );
  Serial.println( smartobj.stream );
  Serial.println();

  Serial.println( F("Yucca User") );
  Serial.print( F("Username : ") );
  Serial.println( user.username );
  Serial.print( F("Password : ") );
  Serial.println( user.password );
  Serial.println();

  Serial.println( F("Yucca Server") );
  Serial.print( F("Address : ") );
  Serial.println( server.addr );
  Serial.print( F("Port    : ") );
  Serial.println( server.port );
  Serial.print( F("Service : ") );
  Serial.println( server.service );
  Serial.println();
*/


  // Initialize Network
#ifdef WIFI
  watchdog_reset();
  Serial.print ( F("Initialing WiFi pins ") );
  Serial.print ( WIFI_EN );
  Serial.print ( F(" : WIFI_EN, ") );
  pinMode(WIFI_EN, OUTPUT);
  pinMode(WIFI_RESET, OUTPUT);  
  Serial.println ( F("ok") );
  
  Serial.println ( F("Turn on WiFi") );  
  network_enabled = wifi_turn_on();
  
  if ( !network_enabled )
  {
    Serial.println( F("WiFi not present") );
  }
  else
  {
    //Serial.println( F("WiFi shield OK") );
    watchdog_reset();
    wifi_connect(ssid, pass);
  }
  

  if (WiFi.status() == WL_CONNECTED)
  {
    network_connected = true;

    // you're connected now, so print out the data:
    Serial.println( F("You're connected to the network" ));
    printCurrentNet();
    printWiFiData();
  }
  else
  {
    Serial.println( F("You're not connected to the network" ));
    network_connected = false;
  }
#else
  watchdog_reset();
  network_connected = true;
  Serial.print( F("Configuring Ethernet using DHCP ... ") );
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println( F("failed") );
    // no point in carrying on, so do nothing forevermore:
  }
  else
  {
    Serial.println( F("done") );
  }
  Serial.print( F("IP address: ") );
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print( F(".") );
  }
  Serial.println();
#endif

  watchdog_reset();
  if (network_connected)
  {
    Serial.print( F("Update time from NTP server ... ") );
    tnpt = 0;
    if ( ntpupdate() )
    {
      Serial.println( F("done") );
      uptime = now();
      tnpt = millis();
    }
    else
    {
      Serial.println( F("error") );
    }
  }
  
}

void loop() {
  time_t t0 = millis() + T_SAMPLE;
/*  
  // Update system-time every 4 hours
  if ( ( millis() - tnpt ) > 10*1000 || year(now()) < 2000 )
  {
    Serial.print( F("Update time from NTP server ... ") );
        
    //setTime((time_t)getNtpTime());
    setSyncProvider(getNtpTime);
    Serial.print( F("done") );
  }
*/

  // Programming reboot every 24 hours
  if ( millis() >= 24*60*60*1000 )
  {
    Serial.println( F("Reboot the system") );
    delay(60000);
  }


  Measure m;

  m.temperature = 0;
  m.humidity = 0;
  m.light = 0;
  m.voc = 0;
  m.co2 = 0;
  
  Serial.println ( F("--------------------------------") );
  
  print_uptime();
  Serial.println( F("Reading sensors.") );

  watchdog_reset();
#ifdef USE_CE_SI7020
  if ( read_CE_SI7020(m.temperature, m.humidity) )
#else
  if ( read_CE_HDC1000(m.temperature, m.humidity) )
#endif  
//  if ( read_CE_HDC1000(m.temperature, m.humidity) )
  {
    if ( verbose_level >= 1 )
    {
      Serial.print( F("Temperature : ") );
      Serial.print(m.temperature);
      Serial.println( F(" C") );
      Serial.print( F("Relative Humidity : ") );
      Serial.print(m.humidity);
      Serial.println( F(" %RH") );
    }
  }

  watchdog_reset();
  if ( read_CE_ISL29003(m.light) )
  {
    if ( verbose_level >= 1 )
    {
      Serial.print( F("Ambient Lux : ") );
      Serial.print(m.light);
      Serial.println( F(" lux") );
    }
  }

  watchdog_reset();
  if ( read_IAQCore(m.voc) )
  {
    if ( verbose_level >= 1 )
    {
      Serial.print( F("VOC: ") );
      Serial.print( m.voc );
      Serial.println( F(" ppm") );
    }
  }

  watchdog_reset();
  if ( read_T6713(m.co2) )
  {
    if ( verbose_level >= 1 )
    {
      Serial.print( F("CO2: ") );
      Serial.print( m.co2 );
      Serial.println( F(" ppm") );
    }
  }

  // **************************************************************************************** 
  watchdog_reset();
  if (timeStatus() != timeNotSet) 
  {
    // Create Yucca json
    print_uptime();
    Serial.println( F("Creating JSON." ));
    
    String json;
    create_json(json, m, smartobj);
    if ( verbose_level >= 2 )
    {
      Serial.println(json);
    }
    //Serial.println(network_enabled);
    //Serial.println(WiFi.status());

    if ( network_connected )
    {
      uint8_t n = 0;
      const uint8_t N_TX = 3;
      bool successful = false;

      while ( !successful && n < N_TX )
      {      
        watchdog_reset();
        print_uptime();
        Serial.print( F("Send JSON[") );
        Serial.print( n );
        Serial.println( F("]") );
        n++;
        
        if ( !yucca_send(server, user, smartobj, json) )
        {
#ifdef WIFI          
          if ( n == (N_TX - 1) || (N_TX - 1) <= 0 )
          {
            // Reset WiFi
            watchdog_reset();
            print_uptime();
            Serial.println( F("Server not connected ... disconnecting from server.") );
            network_client.stop();
                  
            watchdog_reset();
            Serial.println( F("Reset Winc1500.") );
            wifi_reset();
        
            // attempt to connect to WiFi network:
            time_t t = millis();

            //wifi_connect(ssid, pass);
            while ( (WiFi.status() != WL_CONNECTED ) && ( ( millis() - t) < 30000 ) ) 
            {
              print_uptime();
              Serial.print( F("Attempting to connect to SSID: ") );
              Serial.println(ssid);
          
              // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
              status = WiFi.begin(ssid, pass);

#ifndef WIFI_DHCP
              Serial.println( F("Use static address") );
              WiFi.config(ip, dns1, gw, subnet); 
#endif
              // wait 10 seconds for connection:
              for (uint8_t i = 0; i < 10; i++)
              {
                watchdog_reset();
                delay (1000);
              }
            }
          }
#endif          
        }
        else
        {
          print_uptime();
          Serial.println( F("Transmission successful") );
          successful = true;
          unsent = 0;
        }
      }
      if ( n == N_TX )
      {
        unsent++;
        Serial.print( F("Unsent : ") );
        Serial.println( unsent );
      }

    }

    if ( unsent > 10 )
    {
      Serial.println( F("Network error. Reboot system") );
      delay(60000);
    }
  }
  else
  {
    Serial.println( F("Time is no set. Reboot system") );
    delay(60000);
  }
  Serial.println ( F("--------------------------------") );

  watchdog_reset();
  if ( network_connected )
  {
    if ( timeStatus() == timeNotSet )
    {
      print_uptime();
      if ( !ntpupdate() )
      {
        Serial.println( F("Error NTP update") );
      }  
      else
      {
        Serial.println( F("NTP update" ));
        if ( uptime == 0 )
        {
          unsigned long t = millis();
          t /= 1000;
          uptime -= t;
        }
      }    
      Serial.println ( F("--------------------------------") );
    }
  }


  // ****************************************************************************************
  uint16_t T_blink = 1000;
  watchdog_reset();
  print_uptime();
  
  Serial.print( F("Waiting ") );
  Serial.print( T_SAMPLE );
  Serial.println( F("ms") );

  long t1 = t0 - millis();
  while ( t1 > 0 && t1 < T_SAMPLE )
  //for (uint8_t i = 0; i  < (T_SAMPLE / 1000); i++)
  {
    watchdog_reset();
    blink_led3(T_blink / 2);
    delay(T_blink / 2);
/*  
    Serial.print( F("missing ") );
    Serial.print( t1 );
    Serial.println( F("ms") );
*/    
    t1 = t0 - millis();
  }
}



/**************************************************************************************
 * FUNCTION DEFINITION
 **************************************************************************************/
//---------------------------------- LEDs -----------------------------------
void blink_led (uint8_t pin_number, uint16_t time_ms)
{
  watchdog_reset();
  digitalWrite(pin_number, HIGH);

  if ( time_ms < t_watchdog || !t_watchdog )
  {
    delay( time_ms );
  }
  else
  {
    Serial.println( F("Blink time is too big for the watchdog timeout!") );    
    delay( t_watchdog - 100 );
  }

  digitalWrite(pin_number, LOW);  
  watchdog_reset();
}

void blink_led3 (uint16_t time_ms)
{
  watchdog_reset();
  digitalWrite(LED_3, LOW);

  if ( time_ms < t_watchdog || !t_watchdog )
  {
    delay( time_ms );
  }
  else
  {
    Serial.println( F("Blink time is too big for the watchdog timeout!") );    
    delay( t_watchdog - 100 );
  }

  digitalWrite(LED_3, HIGH);
  watchdog_reset();
}

void test_rgb()
{
  uint16_t t = 2000;
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, LOW);
  digitalWrite(LED_BLUE, LOW);

  blink_led(LED_RED, t);
  blink_led(LED_GREEN, t);
  blink_led(LED_BLUE, t);
}



//--------------------------------- Sensors ---------------------------------
bool initalize_CE_SI7020()
{
  // Start I2C transmission
  Wire.beginTransmission(CE_SI7020);
  // Stop I2C transmission
  Wire.endTransmission();
}


bool read_CE_SI7020(float &temperature, float &humidity, uint8_t sensor)
{
  unsigned int data[2] = {0};

  //Serial.print( F("Read T/H Control-Everything sensor [CE_SI7020]") );

  if (sensor == 0 or sensor == 2)
  {
    // Starts I2C communication
    Wire.beginTransmission(CE_SI7020);
    // Send humidity measurement command, NO HOLD MASTER
    Wire.write(0xF5);
    // Stop I2C transmission
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    delay(500);
    
    // Request 2 bytes of data
    Wire.requestFrom(CE_SI7020, 2);
    // Read 2 bytes of data
    // humidity msb, humidity lsb 
    if(Wire.available() == 2)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
    }
    
  // Convert the data
    //float 
    humidity  = ((data[0] * 256.0) + data[3]);
    humidity = ((125 * humidity) / 65536.0) - 6;
  }
  if (sensor == 0 or sensor == 2)
  {  
    // Start I2C transmission
    Wire.beginTransmission(CE_SI7020);
    // Send temperature measurement command, NO HOLD MASTER
    Wire.write(0xF3);
    // Stop I2C transmission
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    delay(500);
    
    // Request 2 bytes of data
    Wire.requestFrom(CE_SI7020, 2);

    // Read 2 bytes of data
    // temp msb, temp lsb
    if(Wire.available() == 2)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
    }

    // Convert the data
    float temp  = ((data[0] * 256.0) + data[1]);
    temperature = ((175.72 * temp) / 65536.0) - 46.85;
    //float fTemp = cTemp * 1.8 + 32;
  }
/*  
  // Output data to serial monitor
  Serial.println ( );
  Serial.print("Relative Humidity :");
  Serial.print(humidity);
  Serial.println(" %RH");
  Serial.print("Temperature in Celsius :");
  Serial.print(cTemp);
  Serial.println(" C");
  Serial.print("Temperature in Fahrenheit :");
  Serial.print(fTemp);
  Serial.println(" F");
*/  
  return true;
}

bool initalize_CE_HDC1000()
{
  // Starts I2C communication
  Wire.beginTransmission(CE_HDC1000);
  // Select configuration register
  Wire.write(0x02);
  // Temperature, humidity enabled, resolultion = 14-bits, heater on
  Wire.write(0x30);
  // Stop I2C Transmission
  return (Wire.endTransmission() == 0);
}

bool read_CE_HDC1000(float &temperature, float &humidity, uint8_t sensor)
{
  unsigned int data[2] = {0};

  //Serial.print( F("Read T/H Control-Everything sensor [HDc1000]") );

  if (sensor == 0 or sensor == 1)
  {
    // Starts I2C communication
    Wire.beginTransmission(CE_HDC1000);
    // Send temp measurement command
    Wire.write(0x00);

    // Stop I2C Transmission
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    delay(500);

    // Request 2 bytes of data
    Wire.requestFrom(CE_HDC1000, 2);

    // Read 2 bytes of data
    // temp msb, temp lsb
    if (Wire.available() == 2)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
    }
    else
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }

    // Convert the data
    int temp = (data[0] * 256) + data[1];
    temperature = (temp / 65536.0) * 165.0 - 40;  // Celsius
    //temperature = cTemp * 1.8 + 32;               // Fahrenheit
  }
   
  if (sensor == 0 or sensor == 2)
  {
    // Starts I2C communication
    Wire.beginTransmission(CE_HDC1000);
    // Send humidity measurement command
    Wire.write(0x01);
    // Stop I2C Transmission
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    delay(500);
    
    // Request 2 bytes of data
    Wire.requestFrom(CE_HDC1000, 2);

    // Read 2 bytes of data
    // humidity msb, humidity lsb
    if (Wire.available() == 2)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
    }
    else
    {
      //Serial.println ( F(" ... error") );
      return false;
    }
    if ( Wire.endTransmission() )
    {
      //Serial.println ( F(" ... error") );
      return false;
    }

    // Convert the data
    humidity = (data[0] * 256) + data[1];
    humidity = (humidity / 65536.0) * 100.0;
  }
  
  /*  
  // Output data to serial monitor
  Serial.println ( );
  Serial.print("Relative Humidity :");
  Serial.print(humidity);
  Serial.println(" %RH");
  Serial.print("Temperature in Celsius :");
  Serial.print(temperature);
  Serial.println(" C");
  //Serial.print("Temperature in Fahrenheit :");
  //Serial.print(temperature);
  //Serial.println(" F");
  */
  return true;
}


bool initalize_CE_ISL29003()
{
  // Starts I2C communication
  Wire.beginTransmission(CE_ISL29003);

  // Select command register
  Wire.write(0x00);
  // Select normal operation
  Wire.write(0x88);
  // Stop I2C Transmission
  if (  (Wire.endTransmission() != 0) )
    return false;

  // Start I2C Transmission
  Wire.beginTransmission(CE_ISL29003);
  // Select control register
  Wire.write(0x01);
  // Set range = 16000 lux
  Wire.write(0x0C);
  // Stop I2C Transmission
  return  (Wire.endTransmission() == 0);
}

bool read_CE_ISL29003(float &light)
{
  unsigned int data[2] = {0};

  //Serial.println( F("Read Lighinting Control-Everything sensor [ISL29003]") );

  // Starts I2C communication
  Wire.beginTransmission(CE_ISL29003);
  // Select data register
  Wire.write(0x04);
  // Stop I2C Transmission
  if ( Wire.endTransmission() )
  {
    //Serial.println ( F(" ... error") );
    return false;
  }

  // Request 2 bytes of data
  Wire.requestFrom(CE_ISL29003, 2);
  // Read 2 bytes of data
  // luminance lsb, luminance msb
  if (Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
  else
  {
    //Serial.println ( F(" ... error") );
    return false;
  }
  
  // Stop I2C Transmission
  if ( Wire.endTransmission() )
  {
    //Serial.println ( F(" ... error") );
    return false;
  }
  delay(300);

  // Convert the data

  float luminance = (data[1] * 256) + data[0];

  light = luminance * 64000;
  light  /= 65535;

  /*
  // Output to the serial monitor
  Serial.print("Ambient Light luminance : ");
  Serial.println(luminance);
  Serial.print("Ambient Lux : ");
  Serial.println(light);
  */
  return true;
}


bool initalize_IAQCore() 
{
  float val = 0;
  return read_IAQCore(val);
}

bool read_IAQCore(float &voc)
{
  uint8_t buf[9] = {0};
  
  if ( I2C_readAllBytes(IAQ, 9, buf, 0) != 9 )
    return false;
  
  bool valid = true;
  uint16_t value = 0;
  uint32_t resistance = 0;
  uint16_t tvoc = 0;
  uint8_t status;
  
  for (uint8_t index = 0; index < 9; index++)
  {
    switch (index)
    {
      case 0 :
      case 1 : 
      {
        value <<= 8;
        value |= buf[index];
      }
        break;
      
      case 2 : 
      {
        status = buf[index];
      }
        break;
        
      case 3 :
      case 4 :
      case 5 :
      case 6 : 
      {
        resistance <<= 8;
        resistance |= buf[index];
      }
        break;
        
      case 7 :
      case 8 : 
      {
        tvoc <<= 8;
        tvoc |= buf[index];
      }
        break;
    }
    /*
    Serial.print( F( "[" ) );
    Serial.print( index );
    Serial.print( F( "] : " ) );
    Serial.println( buf[index] , HEX );
    */
  }
/*
   Serial.print( F("PPM : ") );
   Serial.println( value );

   Serial.print( F("Status : ") );
   Serial.println( status);

   Serial.print( F("Resistance : ") );
   Serial.println( resistance );

   Serial.print( F("tvoc : ") );
   Serial.println( tvoc );
*/   
   voc = value;
   return true;
}

bool initalize_T6713() 
{
  float val = 0;
  return read_T6713(val);
}

bool read_T6713(float &co2)
{
  uint16_t reg;
  uint16_t c = 0x138B;;
  
  //Serial.println(c, HEX);
  uint8_t buf_in[5] = {0};
  uint8_t buf_out[4] = {0};
  
  buf_in[0] = 0x04;
  buf_in[2] = c;
  buf_in[2] &= 0x0FF;
  
  c >>= 8;
  buf_in[1] = c;
  buf_in[1] &= 0x0FF;
  buf_in[3] = 0x00;
  buf_in[4] = 0x01;

  /*
  for (uint8_t i = 0; i < 5; i++)
  {
    Serial.println(buf_in[i], HEX);
  }
  */
      
  if ( !I2C_writeAllBytes(T6713, 5, buf_in, 0) )
    return false;

  //Serial.println( F("Read response"));
  if ( I2C_readAllBytes(T6713, 4, buf_out, 0) != 4 )
    return false;
    
  //Serial.println( F("Process response"));
  bool valid = true;
  for (uint8_t index = 0; index < 4; index++)
  {
/*
            Serial.print( index );
            Serial.print( F(" : "));
            Serial.println( buf_out[index] );
            Serial.println( valid);
*/
    switch (index)
    {
      case 0 : 
      {
        if ( buf_out[index] != 0x04 )
        {
          valid = false;
        }
      }
        break;
      
      case 1 : 
      {
        if ( buf_out[index] != 0x02 )
        {
          valid = false;
        }
      }
        break;
      
      case 2 : 
      { 
        reg = buf_out[index]; 
        reg <<= 8; 
      }
        break;
      
      case 3 : 
      { 
        reg += (uint16_t) (buf_out[index] & 0x0FF) ; 
      }
        break;
      
      default :
        valid = false;
    }
  }
  
  //Serial.println( valid);
  if (!valid)
    return false;

  co2 = reg;
    
  return true;
}

bool I2C_writeAllBytes(uint8_t devAddr, uint8_t length, uint8_t *data, uint16_t timeout)
{
/*
#ifdef I2CDEV_SERIAL_DEBUG
      Serial.print("I2C (0x");
      Serial.print(devAddr, HEX);
      Serial.print(") writing ");
      Serial.print(length, DEC);
      Serial.print(" bytes to 0x");
      //Serial.print(regAddr, HEX);
      Serial.println("...");
#endif
*/
  uint8_t status = 0;
  
  Wire.beginTransmission(devAddr);
  
  for (uint8_t i = 0; i < length; i++)
  {
/*
#ifdef I2CDEV_SERIAL_DEBUG
        Serial.print(data[i], HEX);
        if (i + 1 < length) Serial.print(" ");
#endif
*/
    Wire.write((uint8_t) data[i]);
  }
  
  status = Wire.endTransmission();
/*
#ifdef I2CDEV_SERIAL_DEBUG
      Serial.println(". Done.");
#endif
*/
  return status == 0;
}

int8_t I2C_readAllBytes(uint8_t devAddr, uint8_t length, uint8_t *data, uint16_t timeout)
{
/*
      #ifdef I2CDEV_SERIAL_DEBUG
      Serial.print( F(" I2C (0x") );
      Serial.print(devAddr, HEX);
      Serial.print( F(") reading ") );
      Serial.print(length, DEC);
      Serial.print( F(" bytes...") );
      #endif
*/
  uint8_t count = 0;
  uint32_t t1 = millis();
  
  Wire.beginTransmission(devAddr);
  Wire.requestFrom(devAddr, length);
  
  for (; Wire.available() && ( timeout == 0 ||  (millis() - t1 < timeout) ); count++)
  {
    data[count] = Wire.read();
  }
  Wire.endTransmission();
  
  return count;
}



#ifdef WIFI
//---------------------------------- Wi-Fi ----------------------------------
bool wifi_turn_on()
{
  digitalWrite(WIFI_EN, HIGH);
  return (WiFi.status() != WL_NO_SHIELD);
}

void wifi_turn_off()
{
  WiFi.end();
  digitalWrite(WIFI_EN, LOW);
}

void wifi_reset()
{
  digitalWrite(WIFI_RESET, LOW);
  digitalWrite(WIFI_EN,    LOW);

  WiFi.end();
  //delay(50);
  delay(500);
    
  digitalWrite(WIFI_EN,    HIGH);
  delay(50);
  digitalWrite(WIFI_RESET, HIGH);
  delay(50);
}

bool wifi_connect(char * ssid, char* password)
{
  // Attempt to connect to WiFi network:
  uint8_t n = 0;
  watchdog_reset();


  while ( status != WL_CONNECTED && n < 3) 
  {
    Serial.print( F("Attempting to connect to WPA SSID: ") );
    Serial.println(ssid);

    if ( strlen(password) == 0 )
    {
      // Connect to "No Encryption" network:
      status = WiFi.begin(ssid);

#ifndef WIFI_DHCP
      Serial.println( F("Use static address") );
      WiFi.config(ip, dns1, gw, subnet); 
#endif
    }
    else
    {
      // Connect to WPA/WPA2 network:
      status = WiFi.begin(ssid, pass);
#ifndef WIFI_DHCP
      Serial.println( F("Use static address") );
      WiFi.config(ip, dns1, gw, subnet); 
#endif
    }
    // wait 10 seconds for connection
    for (uint8_t i = 0; i< 10; i ++)
    {
      watchdog_reset();
      delay (1000);
    }
    n++;
  }
  return (status == WL_CONNECTED);
}

void printWiFiData() 
{
  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print( F("IP Address  : ") );
  Serial.println(ip);

  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print( F("MAC address : ") );
  for ( uint8_t i = 5; i > 1; i--)
  {
    Serial.print(mac[i], HEX);
    Serial.print( F(":") );    
  }
  Serial.println(mac[0], HEX);

}

void printCurrentNet() 
{
  // print the SSID of the network you're attached to:
  Serial.print( F("SSID        : ") );
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print( F("BSSID       : ") );
  for ( uint8_t i = 5; i > 1; i--)
  {
    Serial.print(bssid[i], HEX);
    Serial.print( F(":") );    
  }
  Serial.println(bssid[0], HEX);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print( F("RSSI        : ") );
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print( F("Encryption  : ") );
  Serial.println(encryption, HEX);
}
#endif


//--------------------------------- Utils -----------------------------------

void printDigits(int digits){
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print( F(":") );
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}


//--------------------------------- YUCCA -----------------------------------
void create_json(String &json, Measure &m, YuccaObj &smartobj)
{
  char buf[25] = { 0 };
  snprintf(buf, 25, "%d-%02d-%02dT%02d:%02d:%02dZ", year(now()),
        month(now()), day(now()), hour(now()),
        minute(now()), second(now()));

            
  json = "{";  
  json += "\"sensor\":\"";
  json += smartobj.uuid;
  json += "\",\"stream\":\"";
  json += smartobj.stream;
  json += "\",\"values\":[{\"time\":\"";
  json += buf;
  json += "\",\"components\":{";

  json += "\"id\":\"";
  json += smartobj.id;
  json += "\",";

  json += "\"T\":";
  json += m.temperature;
  json += ",";
  json += "\"H\":";
  json += m.humidity;
  json += ",";
  json += "\"lux\":";
  json += m.light;  
  json += ",";
  json += "\"VOC\":";
  json += m.voc;
  if (  m.co2 > 0.0 )
  {
    json += ",";
    json += "\"CO2\":";
    json += m.co2;
  }

  json += "}}]";

  json+="}";
}

bool yucca_send(YuccaServer &server, YuccaUser &user, YuccaObj &smartobj, String &msg)
{
  unsigned int length = 0;

  char *newline = "\r\n";
  length += strlen(msg.c_str());
  String header = "POST ";
  header += server.service;
  header += " HTTP/1.1";
  header += newline;
  header += "Host: ";
  header += server.addr;
  header += newline;
  header += "User-Agent: Arduino/1.0";
  header += newline;
  header += "Content-Type: application/json";
  header += newline;
  header += "Authorization: Basic ";
  header += user.password;
  header += newline;
  header += "Content-Length: ";
  char buf[16] = {0};
  snprintf(buf, 15, "%d", strlen(msg.c_str()));
  header += buf;
  header += newline;
  header += newline;

  watchdog_reset();  
  // if you get a connection, report back via serial:
  if (!network_client.connected()) 
  {
    print_uptime();
    Serial.print( F("Starting connection to server ... ") );
    if (network_client.connect(server.addr.c_str(), server.port)) 
    {
      Serial.println( F("done") );
    }
  }
  // if the server's disconnected, stop the client:
  if (!network_client.connected()) 
  {
    Serial.println( F(" failed") );
    return false;
  }
  else
  {
    // Make a HTTP request:    
    watchdog_reset();
    //Serial.println("HTTP POST");
    byte i = 0;
    size_t n = 0;
    size_t len = strlen(header.c_str());
    while (len > 0)
    {
      watchdog_reset();

      #define B_SIZE 64
      char wBuf[B_SIZE] = {0};
      memset(wBuf, 0, B_SIZE);
      size_t l = (len > B_SIZE - 1) ? B_SIZE - 1 : len;
      memcpy(wBuf, &((header.c_str())[n]), l);
      size_t lw = network_client.print(wBuf);

      if (lw != l)
      {
        network_client.stop();
        return false;
      }
      //Serial.print(wBuf);
      
      len -= l;
      n += l;
      
      //network_client.flush();
    }

    n = 0;
    len = strlen(msg.c_str());
    while (len > 0)
    {
      watchdog_reset();

      char wBuf[B_SIZE] = {0};
      memset(wBuf, 0, B_SIZE);
      
      size_t l = (len > B_SIZE - 1) ? B_SIZE - 1 : len;
      memcpy(wBuf, &((msg.c_str())[n]), l);
      size_t lw = network_client.print(wBuf);

      if (lw != l)
      {
        network_client.stop();
        return false;
      }

      //Serial.print(wBuf);
      len -= l;
      n += l;
      //network_client.flush();
    }
    //Serial.println();

    delay(500);

    String response;    
    while (network_client.available()) 
    {
      watchdog_reset();
      char c = network_client.read();
      response += c;
      //Serial.write(c);
    }

    if (verbose_level == 2 )
    {
      Serial.println( response );
    }


    watchdog_reset();
    print_uptime();
    Serial.println( F("Disconnecting from server.") );
    network_client.stop();

  }
  //Serial.println (".......................");
  return true;
}


//--------------------------------- NTP -------------------------------------
time_t getNtpTime()
{
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  //Serial.println( F("Transmit NTP Request") );
  sendNTPpacket(timeServer);
  
  uint32_t beginWait = millis();
  while (millis() - beginWait < 10000) 
  {
    Watchdog.reset();  

    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) 
    {
      //Serial.println( F("Receive NTP Response") );
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      time_t t = secsSince1900;
      t -= 2208988800UL;
      return t /*+ timeZone * SECS_PER_HOUR*/;
    }
  }
  //Serial.println( F("No NTP Response") );
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address)
{
#ifdef WATCHDOG
  Watchdog.reset();  
#endif
  //Serial.println("1");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  //Serial.println("2");
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  //Serial.println("3");

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  //Serial.println("4");
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  //Serial.println("5");
  Udp.endPacket();
  //Serial.println("6");
#ifdef WATCHDOG
  Watchdog.reset();  
#endif
}

bool ntpupdate()
{ 
  uint8_t n = 0;
  while (( timeStatus() == timeNotSet ) && ( n < NTP_ATTEMPT ) )
  {
    Watchdog.reset();
    Udp.begin(localPort);
    
    //setTime((time_t)getNtpTime());
    setSyncProvider(getNtpTime);
    
    Udp.stop();  
  }

  Watchdog.reset();

  return (timeStatus() != timeNotSet);
}


//------------------------------- Watchdog ----------------------------------
uint16_t watchdog_init(uint16_t t)
{
#ifdef WATCHDOG
  uint16_t countdownMS = Watchdog.enable(t);
  Serial.print( F("Enabled the watchdog with max countdown of ") );
  Serial.print(countdownMS, DEC);
  Serial.println( F(" milliseconds!") );
  Serial.println();

  return countdownMS;
#else
  return 0;
#endif
}

void watchdog_reset()
{
#ifdef WATCHDOG
  Watchdog.reset();  
#endif
}


//-------------------------------- Debug ------------------------------------
void print_uptime()
{
  unsigned long t = millis();

  if ( uptime != 0 )
  {
    t = now();
    t -= uptime;
  }
  else
  {
    t /= 1000;
  }
  Serial.print( F("[") );
  Serial.print( t );
  Serial.print( F("] ") );
}
